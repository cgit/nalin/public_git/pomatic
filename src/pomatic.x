%{

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <sys/time.h>
#include <sys/wait.h>
#include <errno.h>
#include <pwd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

char message[65536];
enum {id, str} string = id, mode = id;
char *msgid = NULL, *msgstr = NULL;
char *filter = "cat";

%}

%%

[ \t]+ {};
^#.* {};
^msgid\  {
	struct passwd *pwd;
	time_t now;
	struct tm tm;
	if (string == str) {
		if (msgid) {
			printf("msgid %s", msgid);
		} else {
			printf("msgid \"\"\n");
		}
		if (strcmp(msgid, "\"\"\n") == 0) {
			pwd = getpwuid(getuid());
			now = time(NULL);
			tm = *(gmtime(&now));
			if (pwd) {
				if (strchr(pwd->pw_gecos, ',')) {
					char *p = strchr(pwd->pw_gecos, ',');
					*p = '\0';
				}
			}
			printf("msgstr \"\"\n");
			printf("\"Project-Id-Version: PACKAGE\\n\"\n");
			printf("\"POT-Creation-Date: %04d-%02d-%02d %02d:%02d-0000\\n\"\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
			printf("\"PO-Revision-Date: %04d-%02d-%02d %02d:%02d-0000\\n\"\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
			printf("\"Last-Translator: %s\\n\"\n", pwd ? pwd->pw_gecos : "the unknown translator");
			printf("\"Language-Team: en_US.%s\\n\"\n", filter);
			printf("\"MIME-Version: 1.0\\n\"\n");
			printf("\"Content-Type: text/plain; charset=UTF-8\\n\"\n");
			printf("\"Content-Transfer-Encoding: 8bit\\n\"\n\n");
		} else
		if ( ((mode == id) && msgid) || ((mode == str) && msgstr) ) {
			int i, ipipe[2], opipe[2];
			pid_t childpid;
			FILE *msgpipe;
			char *text = (mode == id) ? msgid : msgstr;

			pipe(ipipe);
			pipe(opipe);
			childpid = fork();

			if (childpid == 0) {
				/* child */
				dup2(ipipe[0], STDIN_FILENO);
				close(ipipe[1]);
				close(opipe[0]);
				dup2(opipe[1], STDOUT_FILENO);
				exit(execlp(filter, filter, NULL));
			}

			close(ipipe[0]);
			close(opipe[1]);

			write(ipipe[1], text, strlen(text));
			close(ipipe[1]);
			msgpipe = fdopen(opipe[0], "r");

			printf("msgstr \"\"\n");
			while (fgets(message, sizeof(message), msgpipe)) {
				for (i = 0; message[i]; i++) {
					if (i > 0)
					if (message[i] == '"')
					if (message[i - 1] != '\\')
					if (message[i - 1] != '\n')
					if (message[i + 1] != '\n')
						fputc('\\', stdout);
					fputc(message[i], stdout);
					if (message[i] == '\\') {
						switch (message[i + 1]) {
						case 'N':
							message[i + 1] = 'n';
							break;
						case 'R':
							message[i + 1] = 'r';
							break;
						case 'T':
							message[i + 1] = 't';
							break;
						}
					}
				}
			}
			waitpid(childpid, NULL, 0);
			fclose(msgpipe);
		}

		if (msgid)
			free(msgid);
		msgid = NULL;

		if (msgstr)
			free(msgstr);
		msgstr = NULL;
	}
	string = id;
};
^msgstr {
	string = str;
};
\".*\"\n {
	char *tmp;
	char **target = NULL;
	if (string == str) {
		target = &msgstr;
	} else {
		target = &msgid;
	}
	tmp = malloc((*target ? strlen(*target) : 0) + strlen(yytext) + 1);
	strcpy(tmp, *target ? *target : "");
	strcat(tmp, yytext);
	free(*target);
	*target = tmp;
};

%%

int
main(int argc, char **argv)
{
	int base = 0;
	yyin = stdin;
	if (argc == 1) {
		printf("Usage: %s [-s] [potfile] [filter]\n",
		       strrchr(argv[0], '/') ?
		       strrchr(argv[0], '/') + 1 :
		       argv[0]);
		return 1;
	}
	if (argc > 1) {
		if (strcmp(argv[1], "-s") == 0) {
			mode = str;
			base++;
		}
	}
	if (argc > base + 1) {
		yyin = fopen(argv[base + 1], "r");
	}
	if (argc > base + 2) {
		filter = argv[base + 2];
	}
	
	while (yyin && !feof(yyin))
		yylex();

	return 0;
}
