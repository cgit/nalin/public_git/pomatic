Name:		pomatic
Version:	1.1
Release:	1%{?dist}
Summary:	A tool for generating gettext translations

Group:		Development/Tools
License:	GPLv2+
Source0:	pomatic-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
The pomatic tool generates a gettext .po file using a .pot template file and a
specified filter command.  The resulting "translation" is mainly useful for
testing.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE
%{_bindir}/*
%{_mandir}/*/*

%changelog
* Tue Feb  2 2010 Nalin Dahyabhai <nalin@redhat.com>
- revive, update to 1.1
- autotoolize

* Sat Jul 12 2003 Nalin Dahyabhai <nalin@redhat.com>
- convert \N to \n when reading filter output
- handle plural forms in .pot files
- add a sample .spec doc file
- add more scripts

* Fri Aug  3 2001 Nalin Dahyabhai <nalin@redhat.com>
- fixup empty-string translation to include actual header information

* Mon Apr  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- initial CVS checkin
